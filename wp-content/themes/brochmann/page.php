<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Brochmann
 * @since Brochmann 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
					<?php $thumb_id = get_post_thumbnail_id(); $thumbUrl = wp_get_attachment_image_src($thumb_id,'full', true); ?>
					
					<header id="content-title" class="row" <?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>style="background-image:url('<?php echo $thumbUrl[0] ?>');"<?php endif; ?>>

							<div class="container">
								<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
							</div>

					</header><!-- .entry-header -->

					<div id="allthecontent" class="row">

						<div class="container entry-content">
							<?php the_content(); ?>
							<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'brochmann' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
						</div><!-- .entry-content -->

					</div>
					
				</article><!-- #post -->

				<?php comments_template(); ?>
			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>