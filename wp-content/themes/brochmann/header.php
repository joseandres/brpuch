<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Brochmann
 * @since Brochmann 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
	<!-- <link rel="stylesheet" type="text/css" href="<?php// echo get_template_directory_uri(); ?>/css/brochmann.css"> -->
</head>

<body <?php body_class(); ?>>
	<header id="masthead" class="site-header" role="banner">
		<div class="row">
			<div class="container">
				<div class="col-xs-12 col-sm-5 col-md-3">
					<?php if ( get_theme_mod( 'brochmann_logo' ) ) : ?>
						<div class='site-logo'>
							<a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'>
								<img src='<?php echo esc_url( get_theme_mod( 'brochmann_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'>
							</a>
						</div>
					<?php else : ?>
						<h1 class='site-title'><a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><?php bloginfo( 'name' ); ?></a></h1>
					<?php endif;
						$description = get_bloginfo( 'description', 'display' );
						if ( $description || is_customize_preview() ) : ?>
						<p class="site-description"><?php echo $description; ?></p>
					<?php endif; ?>
				</div>
				<div class="col-xs-12 col-sm-7 col-md-9">

					<?php dynamic_sidebar( 'sidebar-3' ); ?>
					<?php// get_search_form(); ?>
					
					<div id="navbar" class="navbar">

						<nav id="site-navigation" class="navigation main-navigation" role="navigation">
							<button class="menu-toggle"><?php _e( 'Menu', 'brochmann' ); ?></button>
							<a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'brochmann' ); ?>"><?php _e( 'Skip to content', 'brochmann' ); ?></a>
							<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu', 'menu_id' => 'primary-menu' ) ); ?>
						</nav><!-- #site-navigation -->

					</div><!-- #navbar -->
				</div>
			</div>
		</div>

	</header><!-- #masthead -->

	<div id="page" class="hfeed site">

		<div id="main" class="site-main">
