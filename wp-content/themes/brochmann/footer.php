<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Brochmann
 * @since Brochmann 1.0
 */
?>

		</div><!-- #main -->
	</div><!-- #page -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="row">
			<div class="container">
				<?php get_sidebar( 'main' ); ?>
				<?php dynamic_sidebar( 'footer-widgets' ); ?>
			</div>
		</div>
	</footer><!-- #colophon -->

	<?php wp_footer(); ?>
</body>
</html>